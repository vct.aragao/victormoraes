@servers(['web' => 'deployer@206.189.216.247'])

@setup
$repository = 'git@gitlab.com:vct.aragao/victormoraes.git';
$releases_dir = '/var/www/victormoraes/releases';
$app_dir = '/var/www/victormoraes';
$release = date('YmdHis');
$new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
setup_app_dir
clone_repository
@endstory

@task('setup_app_dir')
echo 'Checking app dir'
[ -d {{ $app_dir }} ] && echo "Exists" || mkdir {{ $app_dir }}
@endtask

@task('clone_repository')
echo 'Cloning repository'
[ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
cd {{ $new_release_dir }}
git reset --hard {{ $commit }}

echo 'Linking current release'
ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

echo 'Linking wp-config'
ln -nfs {{ $app_dir }}/wp-config.php {{ $new_release_dir }}/wp-config.php
@endtask