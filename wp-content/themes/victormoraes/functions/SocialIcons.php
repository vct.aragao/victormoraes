<?php

class SocialIcons
{

  private static $facebook_icon   =   '<i class="icon fab fa-facebook-f" aria-hidden="true" ></i>';
  private static $instagram_icon   =  '<i class="icon fab fa-instagram"></i>';
  private static $likedin_icon  =  '<i class="icon fab fa-linkedin-in"></i>';
  private static $youtube_icon  =  '<i class="fab fa-youtube"></i>';
  private static $whatsapp_icon   =   '<i class="fab fa-whatsapp"></i>';


  public static function getSocialIcon($social_network)
  {
    if ($social_network == "Facebook" || $social_network == "facebook")
      return self::getFacebookIcon();
    elseif ($social_network == "Instagram" || $social_network == "instagram" || $social_network == "insta")
      return self::getInstagramIcon();
    elseif ($social_network == "Linkedin" || $social_network == "linkedin")
      return self::getLinkedinIcon();
    elseif ($social_network == "Youtube" || $social_network == "youtube")
      return self::getYoutubeIcon();
    elseif ($social_network == "Whatsapp" || $social_network == "whatsapp")
      return self::getWhatsappIcon();
  }

  private static function getFacebookIcon()
  {
    return self::$facebook_icon;
  }

  private static function getInstagramIcon()
  {
    return self::$instagram_icon;
  }

  private static function getLinkedinIcon()
  {
    return self::$likedin_icon;
  }

  private static function getYoutubeIcon()
  {
    return self::$youtube_icon;
  }

  private static function getWhatsappIcon()
  {
    return self::$whatsapp_icon;
  }
}
