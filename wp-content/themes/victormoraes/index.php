<?php get_header(); ?>
<header class="main_header">
  <nav class="main_nav">
    <a target="_blank" class="social_icon" href="https://facebook.com/vctaragao"><?php echo SocialIcons::getSocialIcon('Facebook'); ?></a>
    <div class="logo">
      <a target="_blank" href="<?php bloginfo('url') ?>"><img src="<?php echo get_site_url() . '/wp-content/uploads/2020/04/logo_white.png' ?>" alt=""></a>
    </div>

    <a class="social_icon" href="https://instagram.com/vctaragao"><?php echo SocialIcons::getSocialIcon('Instagram'); ?></a>
  </nav>

  <h1>Victor <span class="second_color">Moraes</span></h1>

  <p class="sub_text_color">Apenas uma pessoa explorando o que significa viver uma vida feliz, enquanto não tenho a resposta completa, passo parte do meu dia desenvolvendo e escrevendo artigos sobre técnologia e aprendizados ao longo da jornadada. Quer saber mais sobre mim? <a href="<?php echo get_site_url() . '/start-here/' ?>" class="second_color">Comece por aqui!</a></p>
</header>

<main class="cards_container">
  <?php
  if (have_posts()) :
    while (have_posts()) : the_post(); ?>
      <div class="card">
        <div class="card_thumbnail">
          <?php the_post_thumbnail() ?>
        </div>
        <h3 class="card_title"><?php the_title() ?></h3>
        <div class="card_divisor">
          <div class="line"></div>
          <small> <?php echo do_shortcode('[rt_reading_time]') . "min de leitura" ?> </small>
        </div>
        <div class="card_body"><?php the_content() ?></div>

        <div class="card_footer">
          <div class="card_author">
            <?php echo get_avatar(get_the_author_meta('ID'), 32); ?>
            <small><span><?php the_author(); ?></span></small>
          </div>
          <a href="<?php the_permalink() ?>">LER MAIS</a>
        </div>
      </div>
  <?php endwhile;
  endif;
  ?>
</main>

<?php get_footer(); ?>