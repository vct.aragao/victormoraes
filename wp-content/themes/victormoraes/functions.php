<?php

add_theme_support('post-thumbnails');

function register_styles_and_scripts()
{
  wp_enqueue_style('material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons');
  wp_enqueue_script('font-awesome', 'https://kit.fontawesome.com/dfb8742735.js', array(), null, true);
  wp_enqueue_script('script', get_template_directory_uri() . '/script.js', array(), null, true);

  wp_enqueue_style('style', get_stylesheet_uri());
}

add_action('wp_enqueue_scripts', 'register_styles_and_scripts');

/**
 * Register menus
 */
function register_menus()
{
  register_nav_menus(
    array(
      'header'      =>  __('Barra de navegação'),
      'social_header' => __('Barra de navegação social'),
      'footer'      =>  __('Rodapé')
    )
  );
}
add_action('init', 'register_menus');

/**
 * Get nav menu items by location
 *
 * @param $location The menu location id
 */
function get_nav_menu_items_by_location($location, $args = [])
{

  // Get all locations
  $locations = get_nav_menu_locations();

  // Get object id by location
  $object = wp_get_nav_menu_object($locations[$location]);

  // Get menu items by menu name
  $menu_items = wp_get_nav_menu_items($object->name, $args);

  // Return menu post objects
  return $menu_items;
}

require_once(__DIR__ . '/functions/SocialIcons.php');
