<?php

/**
 * Template Name: start-here
 */

get_header(); ?>

<header class="start_here">
  <nav class="single_nav">
    <div class="full_logo">
      <a href="<?php bloginfo("url") ?>"><img src="http://localhost/victormoraes/wp-content/uploads/2020/04/logo_full_white.png" alt=""></a>
    </div>
    <ul>
      <?php $menu_items = get_nav_menu_items_by_location('header');
      foreach ($menu_items as $item) : ?>
        <li>
          <a href="<?php echo $item->url ?>"><?php echo $item->post_title ?></a>
        </li>
      <?php endforeach ?>
    </ul>
  </nav>
</header>
<main class="start_here_main">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

      <h1><?php the_title(); ?> <span class="material-icons">sentiment_satisfied_alt</span></h1>

      <img src="<?php the_post_thumbnail_url() ?>" alt="imagem de perfil">

      <?php the_content(); ?>

    <?php endwhile; ?>
  <?php endif; ?>

</main>

<?php get_footer(); ?>