
let COL_COUNT = 0;  // set this to however many columns you want
if (screen.width >= 1200) COL_COUNT = 3;
else if (screen.width >= 780) COL_COUNT = 2;
else COL_COUNT = 1;

console.log(COL_COUNT);

let col_heights = [];

if (COL_COUNT > 1) {
  let container = document.querySelector('.cards_container');
  for (let i = 0; i <= COL_COUNT; i++) {
    col_heights.push(0);
  }
  for (let i = 0; i < container.children.length; i++) {
    let order = (i + 1) % COL_COUNT || COL_COUNT;
    container.children[i].style.order = order;
    if (order === 1) container.children[i].classList.add('fadein_fast');
    else if (order === 2) container.children[i].classList.add('fadein_medium');
    else container.children[i].classList.add('fadein_slow');
    col_heights[order] += parseFloat(container.children[i].clientHeight) + 48;
  }

  let highest = Math.max.apply(Math, col_heights) + 48;
  container.style.height = highest + 'px';
}

