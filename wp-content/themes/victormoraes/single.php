<?php get_header() ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <header class="single_header">
      <nav class="single_nav">
        <div class="full_logo">
          <a href="<?php bloginfo("url") ?>"><img src="<?php echo get_site_url() . '/wp-content/uploads/2020/04/logo_full_white.png' ?>" alt=""></a>
        </div>
        <ul>
          <?php $menu_items = get_nav_menu_items_by_location('header');
          foreach ($menu_items as $item) : ?>
            <li>
              <a href="<?php echo $item->url ?>"><?php echo $item->post_title ?></a>
            </li>
          <?php endforeach ?>
        </ul>
      </nav>
      <img class="background_image" src="<?php the_post_thumbnail_url() ?>" alt="">
      <h1>
        <?php the_title(); ?>
      </h1>
      <div class="post_info">
        <div class="post_author">
          <?php echo get_avatar(get_the_author_meta('ID'), 32); ?>
          <small><span><?php the_author(); ?></span></small>
        </div>
        <small><?php the_date('d/m/Y') ?></small>
      </div>
    </header>

    <main class="single_main">
      <?php the_content() ?>
    </main>
  <?php endwhile; ?>
<?php endif; ?>
<?php get_footer() ?>