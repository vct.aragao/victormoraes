<footer>
  <div class="owner_info">
    <span>Victor Moraes @ 2020</span>
    <span>(35) 9 9102-9749</span>
  </div>

  <ul class="footer_menu">
    <?php $menu_items = get_nav_menu_items_by_location('header');
    foreach ($menu_items as $item) : ?>
      <li>
        <a href="<?php echo $item->url ?>"><?php echo $item->post_title ?></a>
      </li>
    <?php endforeach ?>
  </ul>

  <?php wp_footer(); ?>
</footer>
</body>

</html>